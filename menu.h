///*////////////////////////////////////////////////////////*/
///*///////////////BUTTON _ ASSIGNMENT//////////////////////*/
///*////////////////////////////////////////////////////////*/
#define btnRIGHT  0
#define btnUP     1
#define btnDOWN   2
#define btnLEFT   3
#define btnSELECT 4
#define btnNONE   5

/*////////////////////////////////////////////////////////*/

/*////////////////////////////////////////////////////////*/
/*///////////////TIME _ ASSIGNMENT////////////////////////*/
/*////////////////////////////////////////////////////////*/
typedef struct Dates {
  uint8_t day;   //default date day
  uint8_t month; //default date month
  uint8_t year;  //default date year
}Date;



typedef struct Times{
  uint8_t hour;  //default time hour
  uint8_t minute;   //default time minites
}Time;


int read_buttons(int key);

void drawMenu ();

void mainMenu(); 

void menu_set_date();

void menu_set_time();

void menu_ph_calibr();

void menu_set_ph();

void menu_watering_times();

void menu_system_ready();

void print_date();

void print_time ();


void value_check (uint16_t x); // save time by having a function to check if x needs a zero



 
  
  