#include <LiquidCrystal.h>
#include <Wire.h>
#include "menu.h"

LiquidCrystal lcd (8, 9, 4, 5, 6, 7);

/*////////////////////////////////////////////////////////*/

/*////////////////////////////////////////////////////////*/
/*///////////////TIME _ ASSIGNMENT////////////////////////*/
/*////////////////////////////////////////////////////////*/


Date date_t;

Time time_t;

/*////////////////////////////////////////////////////////*/

/*////////////////////////////////////////////////////////*/
/*///////////////MENU _ ASSIGNMENT////////////////////////*/
/*////////////////////////////////////////////////////////*/
// Menu item input
String menuItems[] = {"SET DATE        ", "SET TIME        ", 
                      "SET pH LEVEL    ", "pH METER CALIBR.", 
                      "WATERING TIMES  ", "SYSTEM READY?   " };
int menu_size = 5;

/*////////////////////////////////////////////////////////*/
/*////////////////////////////////////////////////////////*/
/*///////////////BUTTON_ CHECK////////////////////////////*/
/*////////////////////////////////////////////////////////*/

int read_buttons(int key)
{
  int key_pressed = 0;
  if      (key < 50)   { key_pressed = btnRIGHT;} // ;return btnRIGHT    // 0
  else if (key < 195)  { key_pressed = btnUP;}  // ;return btnUP;      // 100
  else if (key < 380)  { key_pressed = btnDOWN;} // ;return btnDOWN;    // 257
  else if (key < 555)  { key_pressed = btnLEFT;} // ;return btnLEFT;    // 411
  else if (key < 790)  { key_pressed = btnSELECT;} // ;return btnSELECT;  // 640
  else if (key > 1000) { key_pressed = btnNONE;} // ;return btnNONE;    // 1024
  return key_pressed; // give out the key pressed
}

void setup()
{
  
  lcd.begin(16,2);
  Serial.begin(9600); 
}

void loop()
{
  drawMenu();
}

void drawMenu ()
{ 
  //Date
   print_date();
  /******************************************************************************/
   //Time
   print_time();
  /******************************************************************************/

  //Temperature
  //lcd.print("Temp:78");
  
  /******************************************************************************/
  lcd.setCursor(1,1);
  lcd.print("1000");
  lcd.setCursor(8,1);
  lcd.print("7.3");
  lcd.setCursor(13,1);
  lcd.print("78");
  /******************************************************************************/
  
  if (read_buttons(analogRead(0)) == btnSELECT) // check if select button has been pressed to enter menu
  { 
    delay(300);
    lcd.clear();
    mainMenu();     
  }
}

/*****************************************************************************************************************/
/****************************************MAIN MENU ***************************************************************/
/*****************************************************************************************************************/
void mainMenu() 
{
  int select = 0; 
  signed int i = 0;
  while (select == 0)
  {
    int button = 0;
    lcd.setCursor(0,0);
    lcd.print("---Main  Menu---");
    lcd.setCursor(0,1);
    
    button = read_buttons(analogRead(0));
    switch(button)
    {
     case 0:
      switch(i)
      {
        case(0):
          delay(250);
          menu_set_date(); //Set Date
          break;
        case(1):
          delay(250);
          menu_set_time(); // Set Time
          break;
        case(2):
          delay(250);
          menu_set_ph(); // pH METER CALIBR    
          break;
        case(3):
          delay(250);
          menu_ph_calibr(); // SET pH LEVEL
          break;
        case(4):
          delay(250);
          menu_watering_times(); // SET pH LEVEL
        break;
         case(5):
          delay(250);
          menu_system_ready(); // SET pH LEVEL
        break;
      }
      break;
      
     case 1: // BUTTON UP Increment Menu
      delay(150);
      if ( (i - 1) < 0 )
      {
        i = 0;
      }
      else 
      {
        i--;
      }
      break; 
      
     case 2: // BUTTON DOWN Decrement Menu
      delay(150); 
      if (menu_size < (i + 1))
      {
        i = menu_size;
      }
      else 
      {
        i++;
      }
      break;
     case 3:  // LEFT Exit out of the menu
      delay(150);
      select = 1;
      lcd.clear();
      break;
     case 4: // Select is being pressed do nothing
       break;
     case 5:
      lcd.print(menuItems[i]);
      delay(150);
      break;
    }
  }
}
/*****************************************************************************************************************/
/****************************************** END _ MAIN_MENU ******************************************************/
/*****************************************************************************************************************/


/*****************************************************************************************************************/
/******************************************* SET_DATE ************************************************************/
/*****************************************************************************************************************/
void menu_set_date()
{
  lcd.clear();
  uint8_t date_edit_day = 0;
  uint8_t date_edit_month = 0;
  uint8_t date_edit_year = 0;
  uint8_t button = 0;
  
  while (date_edit_day == 0)  /********************************************************************************DAY*/
  {
    lcd.setCursor(0,0);
    lcd.print("Set Date Menu");
    lcd.setCursor(0,1);
    lcd.print("Day:");
    lcd.setCursor(4,1);
    button = read_buttons(analogRead(0));
    switch(button)
    {
      case 0: //RIGHT Next variable month
        delay(250);   
        lcd.clear();
        date_edit_month = 1; 
        while (date_edit_month == 1)   /**********************************************************************MONTH*/
        {
          lcd.setCursor(0,0);
          lcd.print("Set Date Menu");
          lcd.setCursor(0,1);
          lcd.print("Month:");      
          button = read_buttons(analogRead(0));
          switch (button)
          {
            case 0: // RIGHT
            delay(250);
            lcd.clear();
            date_edit_year = 1;
            while(date_edit_year == 1)   /**********************************************************************YEAR*/
            {
              lcd.setCursor(0,0);
              lcd.print("Set Date Menu");
              lcd.setCursor(0,1);
              lcd.print("Year:");
              button = read_buttons(analogRead(0));
              switch(button)
              {
                case btnRIGHT: //RIGHT
                break;
                
                case btnUP:
                   delay(150);
                   date_t.year = date_t.year + 1;
                break;
                
                case btnDOWN:
                   delay(150);
                   date_t.year = date_t.year - 1;
                break;
                
                case btnLEFT:
                  delay(150);
                  lcd.clear();
                  date_edit_year = 0;
                break;
                case btnSELECT:
                  // END LOOP and save date
                  delay(150);
                  return;
                break;
                
                case btnNONE:
                  //Show year
                  delay(100);
                  value_check(date_t.year); 
                break;
              }
              
            }
            //  lcd.setCursor(7,1);
            break;
              
            case 1: // UP
            delay(150);
              if((date_t.month + 1) >= 12)
              {
                date_t.month = 1;
              }
              else 
              {
                date_t.month = date_t.month + 1;
              }
            break;
            
            case 2: // DOWN
            delay(150);
              if((date_t.month - 1) <= 1){
                date_t.month = 12;
              }
              else{
                date_t.month = date_t.month - 1;
              }
            break;
            
            case 3: // LEFT
              delay(150);
              lcd.clear();
              date_edit_month = 0;
            break;
            
            case 4: // SELECT
            break;
            
            case 5: // NONE
            delay(100);
            value_check(date_t.month);
            break;
          }
          
        }
        break; // Right, next value
         
      case 1: //UP
        delay(150);
        if( (date_t.day + 1) >= 31)
        {
          date_t.day = 1;
        }
        else
        {
          date_t.day = date_t.day + 1;
        }
        break; // increment value i and show
        
      case 2: //DOWN
        delay(150);
        if ((date_t.day - 1) <= 1)
        {
          date_t.day = 31;
        }
        else
        {
          date_t.day = date_t.day - 1;
        }
        break; // decrement value i and show
        
      case 3: //LEFT
        delay(150);
        date_edit_day = 1;
        break; // break out of menu
        
      case 4: //RIGHT
        break; // Select do nothing at the moment
        
      case 5: // NOTHING
       delay(150);
       value_check(date_t.day);
        break; // Nothing is pressed no nothing
    }
  }
}

/*****************************************************************************************************************/
/************************************** END_SET_DATE *************************************************************/
/*****************************************************************************************************************/



/*****************************************************************************************************************/
/************************************** SET_TIME******************************************************************/
/*****************************************************************************************************************/
void menu_set_time()
{
  lcd.clear();
  Serial.println("EDIT TIME ");
  int8_t time_edit_hour = 0;
  int8_t time_edit_minute = 0;
  uint8_t button = 0;
  while(time_edit_hour == 0)
  {
    lcd.setCursor(0,0);
    lcd.print("Set Time Menu");
    lcd.setCursor(0,1);
    lcd.print("Hour:");
    lcd.setCursor(5,1);
    button = read_buttons(analogRead(0));
    switch(button)
    {
      case btnRIGHT:
        time_edit_minute = 1;
        while(time_edit_minute == 1)
        {
          lcd.setCursor(0,0);
          lcd.print("Set Time Menu");
          lcd.setCursor(0,1);
          lcd.print("Minute:");
          lcd.setCursor(7,1);
          button = read_buttons(analogRead(0));
          switch(button)
          {
            case btnRIGHT:
            break;
            
            case btnUP:
              delay(150);
                if ((time_t.minute + 1) >= 61)
                {
                  time_t.minute = 0;
                }
                else
                {
                  time_t.minute = time_t.minute + 1;
                }
              break;
              
              case btnDOWN:
              delay(150);
               if ((time_t.minute - 1) <= (-1))
                {
                  time_t.minute = 60;
                }
                else
                {
                  time_t.minute = time_t.minute - 1;
                }
              break;
              
              case btnLEFT:
              delay(150);
              time_edit_minute = 0;
              break;
              
              case btnSELECT:
                delay(150);
                return;
              break;
              
              case btnNONE:
                delay(150);
                value_check(time_t.minute);
                Serial.println("TIME _ TIME");
              break;
          }
        }
      break;
      case btnUP:
      delay(150);
        if ((time_t.hour + 1) >= 24)
        {
          time_t.hour = 0;
        }
        else
        {
          time_t.hour = time_t.hour + 1;
        }
      break;
      case btnDOWN:
      delay(150);
       if ((time_t.hour - 1) <= (-1))
        {
          time_t.hour = 23;
        }
        else
        {
          time_t.hour = time_t.hour - 1;
        }
      break;
      case btnLEFT:
      delay(150);
      time_edit_hour = 1;
      break;
      case btnSELECT:
      break;
      case btnNONE:
      delay(150);
      value_check(time_t.hour);
      break;
    }
  }
 
}
/*****************************************************************************************************************/
/************************************** END_SET_TIME**************************************************************/
/*****************************************************************************************************************/
void menu_ph_calibr()
{
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Set pH Level Menu");
  delay(150);
}
void menu_set_ph()
{
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("pH Calibr. Menu");
  delay(150);
}

void menu_watering_times()
{
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Watering Menu");
  delay(150);
}

void menu_system_ready()
{
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("System Ready Menu");
  delay(150);
}

void print_date()
{
  lcd.setCursor(0,0);
  value_check(date_t.day);
  lcd.print("/");
  value_check(date_t.month);
  lcd.print("/");
  value_check(date_t.year);
}

void print_time ()
{
  lcd.setCursor(11,0);
  value_check(time_t.hour);
  lcd.print(":");
  value_check(time_t.minute);
}

void value_check (uint16_t x) // save time by having a function to check if x needs a zero
{
  if( x < 10)
  {
    lcd.print('0');
    lcd.print(x);
  }
  else
   {
    lcd.print(x);
   }
}



 
  
  